package fr.qcqteam.modular.eco;

import fr.qcqteam.modular.command.utils.CommandInfo;
import fr.qcqteam.modular.command.utils.ICommand;
import fr.qcqteam.modular.command.utils.ICommandArg;
import org.bukkit.command.CommandSender;

@ICommand(commandName = "HelloThere", usage = "/hellothere")
public class EcoCommand {

    @ICommandArg(description = "Hello There")
    public void hello(TestCommandInfo infos){
        infos.getCommandSender().sendMessage("Hello Man It Works : " + infos.getName() + " > " + infos.getBank());
    }

}
