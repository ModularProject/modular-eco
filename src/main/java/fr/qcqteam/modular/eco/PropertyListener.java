package fr.qcqteam.modular.eco;

import fr.qcqteam.modular.event.InfoInitEvent;
import fr.qcqteam.modular.player.PlayerInfo;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PropertyListener implements Listener {

    @EventHandler
    @SuppressWarnings("unchecked")
    public void onPlayerInfoConstruct(InfoInitEvent.Pre event) {
        if (event.getInfo() instanceof PlayerInfo) {
            event.getInfo().addProperty(new EcoProperty());
        }
    }
}
