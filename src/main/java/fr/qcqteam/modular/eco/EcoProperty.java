package fr.qcqteam.modular.eco;

import fr.qcqteam.modular.info.property.SavedItem;
import fr.qcqteam.modular.player.PlayerExtendedProperty;
import fr.qcqteam.modular.player.PlayerInfo;
import fr.qcqteam.modular.utils.Logger;
import lombok.Getter;

public class EcoProperty extends PlayerExtendedProperty<PlayerInfo> {

    public static final String PROPERTY_TAG = "eco_property";

    @Getter
    @SavedItem()
    private int purseAmount;

    @Getter
    @SavedItem()
    private int bankAmount;

    @Getter
    @SavedItem()
    private Volt volt;


    @Override
    public void init() {
        this.purseAmount = 0;
        this.bankAmount = 0;
        this.volt = new Volt();
        Logger.info("Init EcoProperty");
    }

    @Override
    public void preLoad() {

    }

    @Override
    public void postLoad() {
        Logger.info("PostLoad EcoProperty " + purseAmount + " / " + bankAmount + " / " + volt);
    }

    @Override
    public void preSave() {

    }

    @Override
    public void postSave() {
        Logger.info("PostSave EcoProperty");
    }

    @Override
    public void setDefault() {
        Logger.info("Default values");
        this.bankAmount = 50;
        this.volt = new Volt("Joli Coffre", 250);
    }

    @Override
    public String getPropertyTag() {
        return PROPERTY_TAG;
    }

    public void addToPurse(int i){
        if(i > 0) {
            this.purseAmount += i;
        }
    }

    public void removeFromPurse(int i){
        if(i > 0) {
            this.purseAmount -= i;
        }
    }

}
