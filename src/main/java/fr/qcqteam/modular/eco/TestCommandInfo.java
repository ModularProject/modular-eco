package fr.qcqteam.modular.eco;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.ModularPlugin;
import fr.qcqteam.modular.command.utils.CommandInfo;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Created by HoxiSword on 04/10/2019 for modular
 */
public class TestCommandInfo extends CommandInfo<Player> {

    public TestCommandInfo(CommandSender sender, String[] arguments) {
        super(sender, arguments);
    }

    public String getName(){
        return getCommandSender().getName();
    }

    public int getBank() {
        return ((EcoProperty)ModularCore.get().getDataManager().get().getPlayerInfo(getCommandSender().getUniqueId().toString()).getProperty(EcoProperty.PROPERTY_TAG)).getBankAmount();
    }
}
