package fr.qcqteam.modular.eco;

import fr.qcqteam.modular.database.sql.ITableReference;
import fr.qcqteam.modular.database.sql.TableReference;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public class Volt implements ITableReference {

    private String name;
    private int size;

    public Volt() {

    }

    public Volt(String name, int size) {
        this.name = name;
        this.size = size;
    }


    @Override
    public TableReference toReference(String table) {
        TableReference reference = new TableReference(table);
        reference.put("name", name);
        reference.put("size", size);
        return reference;
    }

    @Override
    public void fromReference(TableReference reference) {
        if (reference.isEmpty())
            return;
        this.name = reference.getOrDefault("name", "");
        this.size = reference.getOrDefault("size", 0);
    }

    @Override
    public String toString() {
        return "Volt{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
