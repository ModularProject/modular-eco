package fr.qcqteam.modular.eco;

import fr.qcqteam.modular.command.CommandHandler;
import fr.qcqteam.modular.module.IModule;
import fr.qcqteam.modular.player.PlayerPropertyManager;
import fr.qcqteam.modular.utils.ListenerUtils;
import org.bukkit.Bukkit;

@SuppressWarnings("unused")
public class ModularEco extends IModule {

    @Override
    public void onEnable() {
        //ListenerUtils.registerListener(new PropertyListener(), this);
        CommandHandler.registerCommand(EcoCommand.class, this);
        PlayerPropertyManager.get().registerProperty("ModularEco", EcoProperty.PROPERTY_TAG, EcoProperty.class);
    }

    @Override
    public void onDisable() {

    }
}
